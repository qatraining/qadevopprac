#!/bin/bash


echo $(curl http://169.254.169.254/latest/meta-data/public-ipv4) puppet >> /etc/hosts


curl -L -o file.tar.gz https://s3-eu-west-1.amazonaws.com/qa-devops/puppet/installers/puppet-enterprise-2016.5.1-ubuntu-16.04-amd64.tar.gz
tar  -xvf file.tar.gz

sudo ./puppet-enterprise-2016.5.1-ubuntu-16.04-amd64/puppet-enterprise-installer
