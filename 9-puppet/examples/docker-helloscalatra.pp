include 'docker'

  exec {'stop containers':
    command => 'docker rm -f hello-scalatra',
    path => '/usr/bin',
  }
  exec { 'delete image':
    command => 'docker rmi kizzie/hello-scalatra',
    path => '/usr/bin',
  }

docker::image { 'kizzie/hello-scalatra':
  ensure => 'present',
}

#docker run --name hello-scalatra -p 8080:8080 kizzie/hello-scalatra
docker::run { 'hello-scalatra':
  image   => 'kizzie/hello-scalatra',
  ports => ['8080:8080'],
}
